# Step-by-step

Para esta demo se va a estar trbajancon un un cluster Single Node de OpenShift 4.13.30.

## Creando proyecto en OpenShift

Primero vamos a crear un nuevo proyecto desde la consola de OpenShift.

Desde la perspectiva de administrados, en la sección **Home** vamos a **Projects** y damos Click en **Create Project**.

![Screenshot](assets/readme_images/create-project-1.png)

Y vamos a tomar los siguientes campos de muestra.

**Name:** 
`demo-camel`

**Display Name:** 
`Demo Camel - Copy database data`

Y Click en **Create**.

![Screenshot](assets/readme_images/create-project-2.png)

<br>
<br>
<br>

## Configuración Kafka

### Instalando operador AMQ Streams

En la perspectiva de **Administrator**, bajo la categoria de **Operators** abrimos el **OperatorHub**.

Y vamos a filtrar por `AMQ Streams`

![Screenshot](assets/readme_images/operator-hub.png)

Damos Click sobre el elemento e instalamos dando Click en **Install**.

![Screenshot](assets/readme_images/operator-install.png)

Vamos a llenar los campos con el siguiente contenido:

**Update channel:**
`stable`

**Installation mode:**
`A specific namespace on the cluster`

**Installed Namespace:**
`demo-camel`

**Update approval:**
`Automatic`

![Screenshot](assets/readme_images/create-operator.png)

Click en **Create**.

Esperamos a que termine de instalar.

![Screenshot](assets/readme_images/op-ready-1.png)

Si vamos a **Installed Operators** dentro de la sección **Operators** deberiamos ver el **Status** del operador **AMQ Streams** en `Succeeded`

![Screenshot](assets/readme_images/op-ready-2.png)

<br>

### Creando cluster de Kafka

Vamos a **Installed Operators** y abrimos el operador **AMQ Streams**.

![Screenshot](assets/readme_images/show-operator.png)

Damos Click sobre el componente **Kafka** para crear un nueva instancia de un cluster de Kafka.

![Screenshot](assets/readme_images/cluster-kafka.png)

Cambiamos el nombre de `my-cluster` a `my-kafka-cluster-for-demo`. Y damos Click en **Create**.

![Screenshot](assets/readme_images/kafka-cluster-name.png)

Y esperamos a que el **Status** cambie a **Ready**.

![Screenshot](assets/readme_images/kafka-cluster-ready.png)

<br>

### Creando Kafka-UI

Ahora vamos a instalar Kafka-UI, que es una interfaz gráfica que vamos a utilizar para ver algunas configuración del cluster y para consumir los mensajes.

Para la instalación, vamos a la perspectiva de **Developer** y verificamos que estemos en el proyecto correcto: `demo-camel`



![Screenshot](assets/readme_images/check-project.png)

Vamos a rear el template abriendo el editor dentro de la consola de OpenShift, en el icono de más (**+**) en la parte superior derecha de la consola de OpenShift.

![Screenshot](assets/readme_images/open-editor.png)

Y en este editor vamos a pegar el contenido del archivo YAML de Kafka-UI que se encuentra en el siguiente archivo:

&emsp; https://github.com/hodrigohamalho/debezium-database-migration/blob/main/kafka/kafka-ui/kafka-ui-deployment.yaml

![Screenshot](assets/readme_images/copy-kafka-ui.png)

```
kind: Service
apiVersion: v1
metadata:
  name: kafka-ui
  labels:
    app: kafka-ui
    app.kubernetes.io/component: kafka-ui
    app.kubernetes.io/instance: kafka-ui
    app.kubernetes.io/name: kafka-ui
spec:
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
  internalTrafficPolicy: Cluster
  type: ClusterIP
  sessionAffinity: None
  selector:
    app: kafka-ui
---
kind: Route
apiVersion: route.openshift.io/v1
metadata:
  name: kafka-ui
  labels:
    app: kafka-ui
    app.kubernetes.io/component: kafka-ui
    app.kubernetes.io/instance: kafka-ui
    app.kubernetes.io/name: kafka-ui
    app.openshift.io/runtime-version: latest
spec:
  to:
    kind: Service
    name: kafka-ui
    weight: 100
  port:
    targetPort: 8080
  wildcardPolicy: None
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: kafka-ui
  labels:
    app: kafka-ui
    app.kubernetes.io/component: kafka-ui
    app.kubernetes.io/instance: kafka-ui
    app.kubernetes.io/name: kafka-ui
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kafka-ui
  template:
    metadata:
      labels:
        app: kafka-ui
        deploymentconfig: kafka-ui
    spec:
      containers:
        - name: kafka-ui
          image: docker.io/provectuslabs/kafka-ui:latest
          ports:
            - containerPort: 8080
              protocol: TCP
          env:
            - name: KAFKA_CLUSTERS_0_NAME
              value: my-cluster
            - name: KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS
              value: 'my-cluster-kafka-bootstrap:9092'
          imagePullPolicy: Always
      restartPolicy: Always
      terminationGracePeriodSeconds: 30

```

Dentro del Deployment vamos a modificar el valor de la variable de ambiente `KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS` por el nombre del servicio **_bootstrap_** del cluster de kafka. 

Para obtener el nombre de este servicio vamos a los recursos del cluster de Kafka y buscamos un servicio, en **Resources** cuyo nombre temrine en `-kafka-bootstrap`.

Vamos a la configuración del operador **AMQ Streams**

![Screenshot](assets/readme_images/bootstrap-1.png)

Abrimos los detalles del cluster de Kafka que cabamos de crear.

![Screenshot](assets/readme_images/bootstrap-2.png)

Ybuscamos el servicio cuyo nombre termine en `-kafka-bootstrap`.

![Screenshot](assets/readme_images/bootstrap-3.png)

Y lo pegamos en la variable `KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS` dentro del YAML para Kafka-UI.

![Screenshot](assets/readme_images/copy-bootstrap.png)

Ahora damos Click en **Create**.

![Screenshot](assets/readme_images/ui-created.png)

Si vamos a la Topologia, vamos a encontrar un Deployment `kafka-ui`.

![Screenshot](assets/readme_images/ui-topology.png)

Y si abrimos su ruta, vamos a acceder a la interfaz de **Kafka-UI**.

![Screenshot](assets/readme_images/kafka-ui-interface.png)

<br>

### Creando topico de Kafka

Desde los detalles de configuración del operador de **AMQ Streams**, en la perspectova de administrador, en **Installed Operators**.

Vamos a dar Click en **Kafka Topic**, en la pestaña de **Details**, especificamente en **Create instance** en la terjeta **Kafka Topic**.

![Screenshot](assets/readme_images/create-topic.png)

Cambiamos el contenido de los campos, tal que queden de la siguiente manera.

**Name:** 
`example1`

**Labels:**
`strimzi.io/cluster=my-kafka-cluster-for-demo`

Borrando el label que este creado por defaul. Pero dejamos el valor de los demas campos por default.

Y damos click en **Create**.

![Screenshot](assets/readme_images/fields-topic.png)

De manera casi inmediata el **Status** del topico deberia cambiar a **Ready**.

![Screenshot](assets/readme_images/topic-ready.png)

Y también podemos ver este nuevo tópico desde el cliente **Kafka-UI**.

Para esto, vamos a la intefaz de Kafka-UI, y damos Click en **Topics**, del menu de la izquierda.

Aqui vamos a poder visualizar el topico que acabamos de creas, así como algunos topicos internos.

![Screenshot](assets/readme_images/ui-topics.png)

Eso quiere decir que el operador, desde los recursos de OpenShift, automáticamente configuró nuestras instancias de Kafka para agregar este nuevo tópico.

<br>
<br>
<br>

## Configuración de bases de datos

### Creando base de datos PostgreSQL (Ephemeral)

Ahora, como base de datos fuente, vamos a utilizar una base de datos efimera de PostgreSQL que vamos a desplegar sobre OpenShift.

Para esto, vamos a la sección **+Add** desde la perspectiva Developer. Y en la categoria **Developer Catalog** vamos a seleccionar **Database**.

![Screenshot](assets/readme_images/database-catalog.png)

Filtramos por `PostgreSQL` y seleccionamos `PostgreSQL (Ephemeral)`

![Screenshot](assets/readme_images/select-postgres.png)

En la pestaña que abra, damos click en `instantiate Template`.

![Screenshot](assets/readme_images/insta-temp-post.png)

Se llenan los campos con los siguientes datos:

**Namespace:**
`demo-camel`

**Namespace _(The OpenShift Namespace where the ImageStream resides._):**
`openshift`

**Database Service Name:**
`postgresql`

**PostgreSQL Connection Username:**
`user`

**PostgreSQL Connection Password:**
`pass`

**PostgreSQL Database Name:**
`db`

El resto de valores los dejamos por default.Y damos Click en **Create**.


![Screenshot](assets/readme_images/fill-post-template.png)

Despues de unos segundos debería terminar de levantar y mostrarse de color azul oscuro.

![Screenshot](assets/readme_images/postgres-topology.png)

<br>

### Configurando PostgreSQL

Vamos a dar Click sobre el icono que se acaba de crear y damos Click sobre le nombre del pod para abrir sus detalles.

![Screenshot](assets/readme_images/open-pos-pod.png)

Aqui vamos a la **Terminal** y vamos a hacer login con el usuario a la base de datos con el cliente de PostgreSQL.

```
psql -d db -U user
```

![Screenshot](assets/readme_images/postgre-login.png)

Ahora vamos a crear la tabla que vamos a utilizar.
```
CREATE TABLE public.users (id serial4 NOT NULL,name varchar(35) NULL DEFAULT NULL,CONSTRAINT users_pk PRIMARY KEY (id));
```

![Screenshot](assets/readme_images/postgre-create-table.png)

Y con `\dt` podemos confirmar que la tabla se haya creado.

```
\dt
```

![Screenshot](assets/readme_images/postgre-list-tables.png)

Y eso seria todo lo que necesitariamos que hacer sobre la base de datos de PostgreSQL.

Y solo como recordatorio, podemos imprimir todos los renglones de la tabla con el siguiente `SELECT`;

```
SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name = 'student';
```

<br>

### Creando base de datos MariaDB (Ephemeral)

Ahora, como base de datos destino, vamos a utilizar una base de datos efimera de MariaDB.

Para esto, vamos a la sección **+Add** desde la perspectiva Developer. Y en la categoria **Developer Catalog** vamos a seleccionar **Database**.

![Screenshot](assets/readme_images/database-catalog.png)

Filtramos por `MariaDB` y seleccionamos `MariaDB (Ephemeral)`

![Screenshot](assets/readme_images/select-db-maria.png)

En la pestaña que abra, damos click en `instantiate Template`.

![Screenshot](assets/readme_images/insta-maria.png)

Se llenan los campos con los siguientes datos:

**Namespace:**
`demo-camel`

**Namespace _(The OpenShift Namespace where the ImageStream resides._):**
`openshift`

**Database Service Name:**
`mariadb`

**PostgreSQL Connection Username:**
`user`

**PostgreSQL Connection Password:**
`pass`

**PostgreSQL Database Name:**
`db`

El resto de valores los dejamos por default.Y damos Click en **Create**.


![Screenshot](assets/readme_images/fill-maria-template.png)

Despues de unos segundos debería terminar de levantar y mostrarse de color azul oscuro.

![Screenshot](assets/readme_images/maria-topology.png)

<br>

### Configurando MariaDB

Vamos a dar Click sobre el icono de MariaDB que se acaba de crear y damos Click sobre le nombre del pod para abrir sus detalles.

![Screenshot](assets/readme_images/maria-pod.png)

Aqui vamos a la **Terminal** y vamos a hacer login con el usuario a la base de datos con el cliente de MySQL.

```
mysql -u user -p
```
E ingresamos la contraseña, que es: `pass`

![Screenshot](assets/readme_images/maria-login.png)

Ahora, seleccionamos la base de datos que creamos.

```
use db;
```

![Screenshot](assets/readme_images/maria-select-db.png)

Ahora vamos a crear la tabla que vamos a utilizar.

```
CREATE TABLE db.users (id int(11) auto_increment NOT NULL,name varchar(50) DEFAULT NULL NULL,`date` varchar(50) DEFAULT NULL NULL,CONSTRAINT users_pk PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
```

![Screenshot](assets/readme_images/maria-create-table.png)

Y con `SHOW TABLES;` podemos confirmar que la tabla se haya creado.

```
SHOW TABLES;
```

![Screenshot](assets/readme_images/maria-show.tables.png)

Y eso seria todo lo que necesitariamos que hacer sobre la base de datos de MariaDB.

Y solo como recordatorio, podemos imprimir todos los renglones de la tabla con el siguiente `SELECT`;

```
SELECT * FROM users;
```

<br>
<br>
<br>

## Despliegue de rutas con S2I

### Desplegando ruta route-db-to-kafka

Ahora vamos a desplegar la primera ruta de Camel, la que va a exponer un servicio REST y que recibiendo un ID va a tomar el elemento con ese ID correspondiente de la base de datos PostgreSQL y lo va a colocar el el tópico de Kafka llamado `example1`.

Para esto, vamos a la sección **+Add** desde la perspectiva Developer. Y en la categoria **Git Repository** vamos a seleccionar **Import from Git**.

![Screenshot](assets/readme_images/add-git.png)

Y en el campo **Git Repo URL** vamos a pegar el URL del repositoroi remoto, que en el caso de esta ruta es el siguiente:

![Screenshot](assets/readme_images/git-db-to-kafka.png)

**Git Repo URL:**
&emsp; https://gitlab.com/rh-savazque-demo/camel/demo-camel-read-db-to-kafka-to-write-db-quarkus/from-db-to-kafka.git

Y vamos a llenar el resto de campos con el siguiente contenido:

**Application:**
`Create application`

**Application name:**
`camel-route-db-to-kafka`

**Name:**
`route-db-to-kafka`

**Resource type:**
`Deployment`

**Target port:**
`8080`

[`CHECK`] **Create a route**

Y damos Click en **Create**.

![Screenshot](assets/readme_images/fill-git-db-to-kafka.png)

Y despues de unos segundos, se debería terminar de desplegar de forma exitosa. Pintandose el icono del Deployment de color azul oscuro.

![Screenshot](assets/readme_images/route-1-deployed.png)














<br>

### Desplegando ruta route-kafka-to-db

Lo siguiente que se haría sería desplegar la ruta de Camel que va a estar escuchando el tópico de Kafka `example1`, le aplicará una tranformación al emnsaje, agregando un nuevo campo con la fecha y finalmente insertandolo en la base de datos de MariaDB.

Para esto, vamos a la sección **+Add** desde la perspectiva Developer. Y en la categoria **Git Repository** vamos a seleccionar **Import from Git**.

![Screenshot](assets/readme_images/add-git.png)

Y en el campo **Git Repo URL** vamos a pegar el URL del repositoroi remoto, que en el caso de esta ruta es el siguiente:

![Screenshot](assets/readme_images/copy-git-kafka-to-db.png)

**Git Repo URL:**
&emsp; https://gitlab.com/rh-savazque-demo/camel/demo-camel-read-db-to-kafka-to-write-db-quarkus/from-kafka-to-db.git

Y vamos a llenar el resto de campos con el siguiente contenido:

**Application:**
`Create application`

**Application name:**
`camel-route-kafka-to-db`

**Name:**
`route-kafka-to-db`

**Resource type:**
`Deployment`

**Target port:**
(vacio)

[` `] **Create a route**

Y damos Click en **Create**.

![Screenshot](assets/readme_images/fill-kafka-to-db.png)

Y despues de unos segundos, se debería terminar de desplegar de forma exitosa. Pintandose el icono del Deployment de color azul oscuro.

Por lo que finalmente la vista de topología quedaría de la siguiente manera.

![Screenshot](assets/readme_images/route-2-deployed.png)





<br>
<br>
<br>

## Pruebas

Para las siguientes consulatas vamos a utilizar **Postman**, y vamos a necesitar la ruta del primer componente de Camel que desplegamos, el qeu expone los servicios REST.

Para esto, vamos a dar Click sobre el icono del Deployment. Y en **Resources** vamos a poder consultar en **Routes** la ruta de este despliegue.

![Screenshot](assets/readme_images/get-route.png)

### Metodo GET

Vamos a intentar recuperar todos los elementos de la base de datos PostgreSQL, que en este momento debería estar vacía.

Esto lo vamos a hacer haciendo una consulta **Get** hacia el endpoint `user`.

![Screenshot](assets/readme_images/get-postman.png)

Y damos Click en **Send** para hacer la consulta.

![Screenshot](assets/readme_images/get-response.png)

Deberíamos observar que nos devuleve un arreglo vacío.

<br>

### Metodo POST

Luego, vamos a insertar un elemento en la base de datos, apuntando al mismo endpoint, pero haciendo una petición **POST** con un JSON en el _body_ de la petición.

![Screenshot](assets/readme_images/post-postman.png)

En este caso, el _body_ que se esta utilizando es el siguiente:

```
{ "name": "Bruce Wayne"}
```


Y damos Click en **Send** para hacer la consulta.

![Screenshot](assets/readme_images/post-response.png)

Deberíamos observar que nos devuleve un JSON muy similar al que enviamos pero devuelto con un ID, que es el que se le asigno en la base de datos PostgreSQL.

<br>

### Replicando elementos

En este momento tenemos:
- 1 registro en la tabla `users` de la base de datos PostgreSQL.
- El tópico `example1` esta vacío.
- La tabla `users` de la base de datos MariaDB se encuentra vacía.

Vamos a confirmarlo.

Vamos a abrir la interfaz de **Kafka-UI**, y vamos a ir a **Topics** y vamos a seleccionar nuestro tópico, `example1`.

![Screenshot](assets/readme_images/empty-topic.png)

Y en la pestaña de **Messages** deberíamos leer el mensaje `No messages found`.

![Screenshot](assets/readme_images/no-messages-found.png)

Por lo tanto, comprobamos que no tenemos elementos en el tópico de Kafka.

Ahora, regresamos a la topología y vamos a abrir el pod de la base de datos de MariaDB.

![Screenshot](assets/readme_images/open-pod-maria.png)

Y en la terminal vamos a iniciar sesión con el cliente de MySQL y vamos a seleccionar la tabla `users`.

Los comandos son los siguientes:

```
mysql -u user -p
```

Recordando que la contrasela es: `pass`

```
use db;
```

![Screenshot](assets/readme_images/login-maria.png)

Y consultamos los registros de la tabla.

```
SELECT * FROM users;
```

![Screenshot](assets/readme_images/empty-maria.png)

Y el mensaje `Empty set` nos confirma que la base de datos se encuentra vacía.

Ahora, a travez de las rutas de Camel, vamos a mover el registro con ID `1`, que vamos a inficar en el parametro del path de la consulta GET que vamos a hacer desde **Postman**.

![Screenshot](assets/readme_images/get-id-postman.png)

Y damos Click en **Send** para realizar la consulta.

![Screenshot](assets/readme_images/get-id-response.png)

Y debería devolver el mensaje `message sent.`.

De forma que si refrescamos **Kafka-UI** en los los mensjaes del tópico `example1` veremos el contenido del registro número 1, que es el que estamos moviendo.

![Screenshot](assets/readme_images/topic-message.png)

Y si volvemos a hacer el `SELECT` sobre la base de datos MariaDB, veremos el registro con un campo nuevo con la fecha.

![Screenshot](assets/readme_images/maria-new.png)

